package com.galvanize.auto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "autos")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Auto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "model_year")
    private int year;
    private String make;
    private String model;
    private String color;
    @Column(name = "owner_name")
    private String owner;
    @Column(unique = true)
    private String vin;
    @JsonFormat(pattern = "dd.MM.yyyy")
    private Date purchaseDate;

    public Auto(){}

    public Auto(int year, String make, String model, String color, String owner, String vin) {
        this.year = year;
        this.make = make;
        this.model = model;
        this.color = color;
        this.owner = owner;
        this.vin = vin;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public int getYear() {
        return year;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public String getOwner() {
        return owner;
    }

    public String getVin() {
        return vin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Auto)) return false;
        Auto auto = (Auto) o;
        return year == auto.year &&
                Objects.equals(make, auto.make) &&
                Objects.equals(model, auto.model) &&
                Objects.equals(color, auto.color) &&
                Objects.equals(owner, auto.owner) &&
                Objects.equals(vin, auto.vin);
    }

    @Override
    public int hashCode() {
        return Objects.hash(year, make, model, color, owner, vin);
    }

    @Override
    public String toString() {
        return "{" +
                "year:" + year +
                ", make:'" + make + '\'' +
                ", model:'" + model + '\'' +
                ", color:'" + color + '\'' +
                ", owner:'" + owner + '\'' +
                ", vin:'" + vin + '\'' +
                '}';
    }
}
