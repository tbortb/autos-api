package com.galvanize.auto;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/autos")
public class AutosController {

    AutosService autosService;

    public AutosController(AutosService autosService) {
        this.autosService = autosService;
    }

    @GetMapping
    public ResponseEntity<List<Auto>> getAutos(@RequestParam(required = false) String color,
                                           @RequestParam(required = false) String make){

        List<Auto> autos = this.autosService.getAutos(color, make);
        return autos.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(autos);

    }

    @GetMapping("/{vin}")
    public ResponseEntity<Auto> getAutoByVin(@PathVariable String vin){
        Auto auto = this.autosService.getAuto(vin);
        return auto != null ? ResponseEntity.ok(auto) : ResponseEntity.status(204).build();
    }

    @PostMapping
    public ResponseEntity<Auto> postAuto(@RequestBody Auto auto){
        Auto addedAuto = this.autosService.addAuto(auto);
        return ResponseEntity.ok(addedAuto);
    }

    @PatchMapping("{vin}")
    public Auto updateAuto(@PathVariable(required = false) String vin,
                           @RequestBody(required = false) HashMap<String, String> changes){
        return this.autosService.updateAuto(vin, changes);
    }

    @DeleteMapping("{vin}")
    public ResponseEntity deleteAuto(@PathVariable(required = false) String vin){
        return this.autosService.deleteAuto(vin) ?
                ResponseEntity.status(202).build() :
                ResponseEntity.status(204).build();
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void invalidAutoExceptionHandler(InvalidAutoException e){
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void invalidAutoChangeExceptionHandler(InvalidAutoChangeException e){
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void invalidAutoChangeExceptionHandler(InvalidVinException e){
    }
}
