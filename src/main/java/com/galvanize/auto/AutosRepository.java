package com.galvanize.auto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AutosRepository extends JpaRepository<Auto, Long> {
    List<Auto> findByColor(String color);

    List<Auto> findByMake(String make);

    List<Auto> findByColorAndMake(String color, String make);

    Auto findFirstByVin(String vin);
}
