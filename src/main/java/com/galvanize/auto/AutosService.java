package com.galvanize.auto;

import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AutosService {

    private static final List<String> ALLOWED_UPDATE_FIELDS = new ArrayList<>(Arrays.asList("owner", "color"));

    private AutosRepository autosRepository;

    public AutosService(AutosRepository autosRepository) {
        this.autosRepository = autosRepository;
    }

    public List<Auto> getAutos(String color, String make){
        if(color == null && make == null){
            return this.autosRepository.findAll();
        }else if(color == null){
            return this.autosRepository.findByMake(make);
        }else if(make == null){
            return this.autosRepository.findByColor(color);
        }else if(color != null && make != null){
            return this.autosRepository.findByColorAndMake(color, make);
        }else{
            return new ArrayList<Auto>();
        }
    }

    public Auto getAuto(String vin){
        return this.autosRepository.findFirstByVin(vin);
    }

    public Auto addAuto(Auto auto){
        return this.autosRepository.save(auto);
    }

    public Auto updateAuto(String vin, HashMap<String, String> changes) {

        Auto changeCar = autosRepository.findFirstByVin(vin);
        if(changeCar == null){
            throw new InvalidVinException("Vin does not exist");
        }
        if (!changes.keySet().stream().allMatch(k -> ALLOWED_UPDATE_FIELDS.contains(k))){
            throw new InvalidAutoChangeException();
        }

        for(String key : changes.keySet()){
            switch (key) {
                case "owner":
                    changeCar.setOwner(changes.get(key));
                    break;
                case "color":
                    changeCar.setColor(changes.get(key));
                    break;
                default:
                    throw new RuntimeException("Argument " + key + " is not supported");
            }
        }

        return autosRepository.save(changeCar);
    }

    public boolean deleteAuto(String vin) {
        Auto auto = autosRepository.findFirstByVin(vin);

        if (auto != null) {
            autosRepository.delete(auto);
            return true;
        }
        return false;
    }

}
