package com.galvanize.auto;

public class InvalidVinException extends RuntimeException{

    InvalidVinException(String message){
        super(message);
    }
}
