package com.galvanize.auto;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

@TestPropertySource(locations= "classpath:application-test.properties")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AutoApiApplicationTests {

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    AutosRepository autosRepository;

    List<Auto> mockAutos;

    private RestTemplate getRestTemplateForPatchRequests() {
        RestTemplate patchRestTemplate = restTemplate.getRestTemplate();
        HttpClient httpClient = HttpClientBuilder.create().build();
        patchRestTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));
        return patchRestTemplate;
    }

    @BeforeEach
    void setUp(){

        mockAutos = new ArrayList<Auto>();

        String[] colors = {"red", "green", "white", "red", "blue"};
        String[] makes = {"Volkswagen", "Volkswagen", "BMW", "Mercedes", "Audi"};
        for (int i = 0; i < 5; i++){
            mockAutos.add(new Auto(2000 + i,
                    makes[i],
                    "any",
                    colors[i],
                    "Me",
                    "156165" + i));
        }
        autosRepository.saveAll(mockAutos);

    }

    @AfterEach
    void cleanUp(){
        autosRepository.deleteAll();
    }

	@Test
	void contextLoads() {
	}

	@Test
    void getAllCars(){
        ResponseEntity<List> response = restTemplate.getForEntity("/api/autos", List.class);
        Assertions.assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        Assertions.assertFalse(response.getBody().isEmpty());
    }

    @Test
    void getCarsFromEmptyDB() {

        autosRepository.deleteAll();

        ResponseEntity<List> response = restTemplate.getForEntity("/api/autos", List.class);

        Assertions.assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        assertThat(response.getBody()).isNull();
    }

    @Test
    void getRedCarsFromDB() {
        int actualRedCarsAmount = (int) mockAutos.stream()
                .filter(c -> c.getColor().equals("red"))
                .count();

        ResponseEntity<List> response = restTemplate.getForEntity("/api/autos?color=red", List.class);

        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        assertThat(response.getBody()).isNotNull();
        Assertions.assertFalse(response.getBody().isEmpty());
        Assertions.assertEquals(actualRedCarsAmount, response.getBody().size());
    }

    @Test
    void getVolkswagen() throws Exception {
        int actualVolkswagenAmount = (int) mockAutos.stream()
                .filter(c -> c.getMake().equals("Volkswagen"))
                .count();

        ResponseEntity<List> response = restTemplate.getForEntity("/api/autos?make=Volkswagen", List.class);


        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        assertThat(response.getBody()).isNotNull();
        Assertions.assertFalse(response.getBody().isEmpty());
        Assertions.assertEquals(actualVolkswagenAmount, response.getBody().size());

    }

    @Test
    void getAutosGreenVolkswagen(){
        String getColor = mockAutos.get(1).getColor();
        String getMake = mockAutos.get(1).getMake();
        String url = "/api/autos?color=" + getColor + "&make=" + getMake;

        ResponseEntity<List> response = restTemplate.getForEntity(url, List.class);
        Assertions.assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertThat(response.getBody()).isNotNull();
        Assertions.assertFalse(response.getBody().isEmpty());
        Assertions.assertEquals(1, response.getBody().size());
    }

    @Test
    void addAuto(){
        Auto postAuto = new Auto(2005, "Skoda", "Fabia", "white", "Kristin", "MB123");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Auto> request = new HttpEntity<Auto>(postAuto, headers);

        ResponseEntity<Auto> response = restTemplate.postForEntity("/api/autos", request, Auto.class);

        Assertions.assertTrue(response.getStatusCode().is2xxSuccessful());
        Assertions.assertEquals(postAuto.getVin(), response.getBody().getVin());
        Assertions.assertNotNull(autosRepository.findFirstByVin(postAuto.getVin()));
    }

    @Test
    void getCarFromDBByVin(){
        Auto getAuto = mockAutos.get(1);
        String url = "/api/autos/" + getAuto.getVin();

        ResponseEntity<Auto> response = restTemplate.getForEntity(url, Auto.class);

        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertEquals(getAuto.getYear(), response.getBody().getYear());
        Assertions.assertEquals(getAuto.getVin(), response.getBody().getVin());
        Assertions.assertEquals(getAuto.getMake(), response.getBody().getMake());
        Assertions.assertEquals(getAuto.getOwner(), response.getBody().getOwner());
    }

    @Test
    void getCarFromDBByInvalidVin(){
        String getVin = "DoesNotExist";
        String url = "/api/autos/" + getVin;

        ResponseEntity<Auto> response = restTemplate.getForEntity(url, Auto.class);

        Assertions.assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        Assertions.assertNull(response.getBody());
    }

    @Test
    void changeOwnerInDB() {

        String newOwner = "Johnny";
        Auto auto = mockAutos.get(0);
        HashMap<String, String> changer = new HashMap();
        changer.put("owner", newOwner);

        String url = "/api/autos/" + auto.getVin();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<HashMap<String, String>> request = new HttpEntity<HashMap<String, String>>(changer, headers);

        ResponseEntity<Auto> response = getRestTemplateForPatchRequests().exchange(url,
                HttpMethod.PATCH,
                request,
                Auto.class);

        Auto updatedDBEntry = autosRepository.findFirstByVin(auto.getVin());

        Assertions.assertEquals(newOwner, updatedDBEntry.getOwner());

        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertEquals(newOwner, response.getBody().getOwner());
        Assertions.assertEquals(auto.getVin(), response.getBody().getVin());

    }




    @Test
    void changeColor(){

        String newColor = "NiceColor";
        Auto auto = mockAutos.get(0);
        HashMap<String, String> changer = new HashMap();
        changer.put("color", newColor);

        String url = "/api/autos/" + auto.getVin();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<HashMap<String, String>> request = new HttpEntity<HashMap<String, String>>(changer, headers);

        ResponseEntity<Auto> response = getRestTemplateForPatchRequests().exchange(url,
                HttpMethod.PATCH,
                request,
                Auto.class);

        Auto updatedDBEntry = autosRepository.findFirstByVin(auto.getVin());

        Assertions.assertEquals(newColor, updatedDBEntry.getColor());

        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertEquals(newColor, response.getBody().getColor());
        Assertions.assertEquals(auto.getVin(), response.getBody().getVin());

    }


    @Test
    void changeOwnerAndColor() {
        String newColor = "NiceColor";
        String newOwner = "NewOwner";
        Auto auto = mockAutos.get(0);
        HashMap<String, String> changer = new HashMap();
        changer.put("color", newColor);
        changer.put("owner", newOwner);

        String url = "/api/autos/" + auto.getVin();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<HashMap<String, String>> request = new HttpEntity<HashMap<String, String>>(changer, headers);

        ResponseEntity<Auto> response = getRestTemplateForPatchRequests().exchange(url,
                HttpMethod.PATCH,
                request,
                Auto.class);

        Auto updatedDBEntry = autosRepository.findFirstByVin(auto.getVin());

        Assertions.assertEquals(newColor, updatedDBEntry.getColor());
        Assertions.assertEquals(newOwner, updatedDBEntry.getOwner());

        Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assertions.assertEquals(newColor, response.getBody().getColor());
        Assertions.assertEquals(newOwner, response.getBody().getOwner());
        Assertions.assertEquals(auto.getVin(), response.getBody().getVin());

    }


    @Test
    void changeModelNotPossible() {
        String nonExistingField = "CanNotChangeThis";
        String newColor = "NiceColor";
        String newOwner = "NewOwner";
        Auto auto = mockAutos.get(0);
        HashMap<String, String> changer = new HashMap();
        changer.put("color", newColor);
        changer.put(nonExistingField, newOwner);

        String url = "/api/autos/" + auto.getVin();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<HashMap<String, String>> request = new HttpEntity<HashMap<String, String>>(changer, headers);

        ResponseEntity<Auto> response = getRestTemplateForPatchRequests().exchange(url,
                HttpMethod.PATCH,
                request,
                Auto.class);

        Auto updatedDBEntry = autosRepository.findFirstByVin(auto.getVin());

        Assertions.assertEquals(auto.getColor(), updatedDBEntry.getColor());
        Assertions.assertEquals(auto.getOwner(), updatedDBEntry.getOwner());

        Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        Assertions.assertNull(response.getBody());

    }

    @Test
    void changeInvalidVinNotPossible(){
        String newColor = "NiceColor";
        Auto auto = mockAutos.get(0);
        HashMap<String, String> changer = new HashMap();
        changer.put("color", newColor);

        String url = "/api/autos/DoesNotExist";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<HashMap<String, String>> request = new HttpEntity<HashMap<String, String>>(changer, headers);

        ResponseEntity<Auto> response = getRestTemplateForPatchRequests().exchange(url,
                HttpMethod.PATCH,
                request,
                Auto.class);

        Auto updatedDBEntry = autosRepository.findFirstByVin(auto.getVin());

        Assertions.assertEquals(auto.getColor(), updatedDBEntry.getColor());

        Assertions.assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
        Assertions.assertNull(response.getBody());
    }

    @Test
    void deleteCar() {
        String deleteVin = mockAutos.get(0).getVin();


        restTemplate.delete("/api/autos/" + deleteVin);

        Auto deletedAuto = autosRepository.findFirstByVin(deleteVin);

        Assertions.assertNull(deletedAuto);

    }

    @Test
    void deleteNonExistingCar() {
        String deleteVin = "DoesNotExist";

        restTemplate.delete("/api/autos/" + deleteVin);

        Assertions.assertEquals(mockAutos.size(), autosRepository.findAll().size());

    }



}
