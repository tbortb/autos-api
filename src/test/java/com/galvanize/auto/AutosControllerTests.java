package com.galvanize.auto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(AutosController.class)
public class AutosControllerTests {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AutosService autosService;

    ObjectMapper mapper = new ObjectMapper();

    private static List<Auto> mockAutos;

    @BeforeEach
    public void setupTests(){

        mockAutos = new ArrayList<Auto>();

        String[] colors = {"red", "green", "white", "red", "blue"};
        String[] makes = {"Volkswagen", "Volkswagen", "BMW", "Mercedes", "Audi"};
        for (int i = 0; i < 5; i++){
            mockAutos.add(new Auto(2000 + i,
                    makes[i],
                    "any",
                    colors[i],
                    "Me",
                    "156165" + i));
        }
    }

    //GET
    @Test
    void getAllCars() throws Exception {

        when(this.autosService.getAutos(nullable(String.class), nullable(String.class))).thenReturn(mockAutos);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/autos")).
        andExpect(status().isOk()).
        andExpect(jsonPath("$", hasSize(5)));
    }

    @Test
    void getAllCarsWhenNoCarsInDB() throws Exception {

        when(this.autosService.getAutos(nullable(String.class), nullable(String.class))).thenReturn(new ArrayList<Auto>());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/autos")).
                andExpect(status().is(204));
    }

    @Test
    void getRedCars() throws Exception {
        List<Auto> redCars = mockAutos.stream()
                .filter(c -> c.getColor().equals("red"))
                .collect(Collectors.toList());

        when(this.autosService.getAutos(nullable(String.class), nullable(String.class))).thenReturn(redCars);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/autos?color=red")).
                andExpect(status().isOk()).
                andExpect(jsonPath("$", hasSize(redCars.size())));
    }

    @Test
    void getVolkswagen() throws Exception {
        List<Auto> volkswagen = mockAutos.stream()
                .filter(c -> c.getMake().equals("Volkswagen"))
                .collect(Collectors.toList());

        when(this.autosService.getAutos(nullable(String.class), nullable(String.class))).thenReturn(volkswagen);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/autos?make=Volkswagen")).
                andExpect(status().isOk()).
                andExpect(jsonPath("$", hasSize(volkswagen.size())));

    }

    @Test
    void getRedVolkswagen() throws Exception {
        List<Auto> redVolkswagen = mockAutos.stream()
                .filter(c -> c.getColor().equals("red") && c.getMake().equals("Volkswagen"))
                .collect(Collectors.toList());

        when(this.autosService.getAutos(nullable(String.class), nullable(String.class))).thenReturn(redVolkswagen);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/autos?color=red&make=Volkswagen")).
                andExpect(status().isOk()).
                andExpect(jsonPath("$").value(hasSize(redVolkswagen.size())));

    }

    //POST
    @Test
    void postValidCar() throws Exception {

        Auto auto = new Auto(1999, "Volkswagen", "Golf", "blue",
                "me", "16515");

        when(this.autosService.addAuto(nullable(Auto.class))).thenReturn(auto);

        mockMvc.perform(post("/api/autos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(auto)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("make").value("Volkswagen"));

    }

    @Test
    void postInvalidCar() throws Exception {
        Auto auto = new Auto(1999, "Volkswagen", "Golf", "blue",
                "me", "16515");

        when(this.autosService.addAuto(nullable(Auto.class))).thenThrow(InvalidAutoException.class);

        mockMvc.perform(post("/api/autos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(auto)))
                .andExpect(status().is4xxClientError());
    }

    //GET/{vin}
    @Test
    void getCarByVin() throws Exception {
        Auto auto = mockAutos.get(0);
        when(this.autosService.getAuto(nullable(String.class))).thenReturn(auto);
        mockMvc.perform(MockMvcRequestBuilders.get("/api/autos/" + auto.getVin()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("vin").value(auto.getVin()));
    }

    @Test
    void getNonExistingCarByVin() throws Exception {
        when(this.autosService.getAuto(nullable(String.class))).thenReturn(null);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/autos/doesNotExist"))
                .andExpect(status().is(204));

    }

    //PATCH
    @Test
    void changeOwner() throws Exception {
        String newOwner = "Johnny";
        Auto auto = mockAutos.get(0);
        HashMap<String, String> changer = new HashMap();
        changer.put("owner", newOwner);
        auto.setOwner(newOwner);

        when(this.autosService.updateAuto(anyString(), any(HashMap.class))).thenReturn(auto);

        mockMvc.perform(patch("/api/autos/" + auto.getVin())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(changer)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("owner").value(newOwner));

    }

    @Test
    void changeColor() throws Exception{
        String newColor = "beautifulColor";
        Auto auto = mockAutos.get(0);
        HashMap<String, String> changer = new HashMap();
        changer.put("color", newColor);
        auto.setColor(newColor);

        when(this.autosService.updateAuto(anyString(), any(HashMap.class))).thenReturn(auto);

        mockMvc.perform(patch("/api/autos/" + auto.getVin())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(changer)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("color").value(newColor));

    }

    @Test
    void changeOwnerAndColor() throws Exception {
        String newOwner = "Johnny";
        String newColor = "beautifulColor";
        Auto auto = mockAutos.get(0);
        HashMap<String, String> changer = new HashMap();
        changer.put("color", newColor);
        changer.put("owner", newOwner);
        auto.setColor(newColor);
        auto.setOwner(newOwner);

        when(this.autosService.updateAuto(anyString(), any(HashMap.class))).thenReturn(auto);

        mockMvc.perform(patch("/api/autos/" + auto.getVin())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(changer)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("color").value(newColor))
                .andExpect(jsonPath("owner").value(newOwner));

    }

    @Test
    void changeModelNotPossible() throws Exception {
        HashMap<String, String> changer = new HashMap<String, String>();
        changer.put("model", "Ferrari 812");
        String changeVin = mockAutos.get(0).getVin();

        when(this.autosService.updateAuto(eq(changeVin), any(HashMap.class)))
                .thenThrow(InvalidAutoChangeException.class);

        mockMvc.perform(patch("/api/autos/" + changeVin)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(changer)))
            .andExpect(status().is4xxClientError());

    }

    //DELETE
    @Test
    void deleteCar() throws Exception {
        String deleteVin = mockAutos.get(0).getVin();

        when(autosService.deleteAuto(deleteVin)).thenReturn(true);

        mockMvc.perform(delete("/api/autos/" + deleteVin))
                .andExpect(status().is(202));

    }

    @Test
    void deleteNonExistingCar() throws Exception {
        String deleteVin = "DoesNotExist";

        when(autosService.deleteAuto(deleteVin)).thenReturn(false);

        mockMvc.perform(delete("/api/autos/" + deleteVin))
                .andExpect(status().is(204));

    }

}
