package com.galvanize.auto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.AdditionalAnswers;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;


import static org.mockito.Mockito.*;
;

@ExtendWith(MockitoExtension.class)
class AutosServiceTest {

    private AutosService autosService;
    private List<Auto> mockAutos;

    @Mock
    AutosRepository autosRepository;

    @BeforeEach
    void setUp(){
        autosService = new AutosService(autosRepository);

        mockAutos = new ArrayList<Auto>();

        String[] colors = {"red", "green", "white", "red", "blue"};
        String[] makes = {"Volkswagen", "Volkswagen", "BMW", "Mercedes", "Audi"};
        for (int i = 0; i < 5; i++){
            mockAutos.add(new Auto(2000 + i,
                    makes[i],
                    "any",
                    colors[i],
                    "Me",
                    "156165" + i));
        }
    }

    @Test
    void getAutos() {
        when(autosRepository.findAll()).thenReturn(mockAutos);

        List<Auto> autos = autosService.getAutos(null, null);
        Assertions.assertNotNull(autos);
        Assertions.assertEquals(mockAutos.size(), autos.size());
        Assertions.assertEquals(mockAutos.get(3).getVin(), autos.get(3).getVin());
    }

    @Test
    void getRedAutos(){
        List<Auto> redCars = mockAutos.stream()
                .filter(c -> c.getColor().equals("red"))
                .collect(Collectors.toList());

        when(this.autosRepository.findByColor("red")).thenReturn(redCars);

        List<Auto> autos = autosService.getAutos("red", null);
        Assertions.assertEquals(redCars.size(), autos.size());
        Assertions.assertEquals(redCars.get(1), autos.get(1));
    }

    @Test
    void getVolkswagen(){
        List<Auto> volkswagen = mockAutos.stream()
                .filter(c -> c.getMake().equals("Volkswagen"))
                .collect(Collectors.toList());

        when(this.autosRepository.findByMake("Volkswagen")).thenReturn(volkswagen);

        List<Auto> autos = autosService.getAutos(null, "Volkswagen");
        Assertions.assertEquals(volkswagen.size(), autos.size());
        Assertions.assertEquals(volkswagen.get(1), autos.get(1));

    }

    @Test
    void getRedVolkswagen(){
        List<Auto> redVolkswagen = mockAutos.stream()
                .filter(c -> c.getMake().equals("Volkswagen") && c.getColor().equals("red"))
                .collect(Collectors.toList());

        when(this.autosRepository.findByColorAndMake("red", "Volkswagen")).thenReturn(redVolkswagen);

        List<Auto> autos = autosService.getAutos("red", "Volkswagen");
        Assertions.assertEquals(redVolkswagen.size(), autos.size());
        Assertions.assertEquals(redVolkswagen.get(0), autos.get(0));

    }

    @Test
    void addValidAuto() {
        Auto postAuto = mockAutos.get(3);

        when(autosRepository.save(postAuto))
                .thenReturn(postAuto);

        Auto postedAuto = autosService.addAuto(postAuto);
        Assertions.assertEquals(postAuto, postedAuto);
    }

    @Test
    void addInvalidAuto() {
        Auto postAuto = mockAutos.get(3);

        when(autosRepository.save(postAuto))
                .thenThrow(InvalidAutoException.class);

        Assertions.assertThrows(InvalidAutoException.class, () -> autosService.addAuto(postAuto));
    }


    @Test
    void getAutoByValidVin() {
        String validVin = mockAutos.get(0).getVin();

        when(autosRepository.findFirstByVin(validVin)).thenReturn(mockAutos.get(0));

        Assertions.assertEquals(mockAutos.get(0), autosService.getAuto(validVin));
    }

    @Test
    void getAutoByInvalidVin() {
        String invalidVin = "invalidVin";

        when(autosRepository.findFirstByVin(invalidVin)).thenThrow(InvalidVinException.class);

        Assertions.assertThrows(InvalidVinException.class, () -> autosService.getAuto(invalidVin));
    }

    @Test
    void updateAuto() {
        String newColor = "silver";
        String newOwner = "Jacob";

        Auto changeCar = mockAutos.get(1);
        HashMap<String, String> updates = new HashMap<>();
        updates.put("color", newColor);
        updates.put("owner", newOwner);
        String vin = changeCar.getVin();

        when(autosRepository.findFirstByVin(vin)).thenReturn(changeCar);
        when(autosRepository.save(ArgumentMatchers.any(Auto.class))).thenAnswer(AdditionalAnswers.returnsFirstArg());

        Auto updatedAuto = autosService.updateAuto(vin, updates);

        Assertions.assertEquals(newColor, updatedAuto.getColor());
        Assertions.assertEquals(newOwner, updatedAuto.getOwner());
        Assertions.assertEquals(vin, updatedAuto.getVin());

    }


    @Test
    void invalidUpdateAuto() {
        HashMap<String, String> updates = new HashMap<>();
        updates.put("color", "silver");
        updates.put("city", "Berlin");
        String vin = mockAutos.get(0).getVin();

        when(autosRepository.findFirstByVin(vin)).thenReturn(mockAutos.get(0));

        Assertions.assertThrows(InvalidAutoChangeException.class, () -> autosService.updateAuto(vin, updates));
    }

    @Test
    void updateNonExistingAuto() {
        HashMap<String, String> updates = new HashMap<>();
        updates.put("color", "silver");
        updates.put("owner", "Jacob");
        String vin = "DoesNotExist";

        when(autosRepository.findFirstByVin(vin)).thenReturn(null);

        Assertions.assertThrows(InvalidVinException.class, () -> autosService.updateAuto(vin, updates));
    }

    @Test
    void deleteAuto() {
        Auto deleteAuto = mockAutos.get(0);

        when(autosRepository.findFirstByVin(deleteAuto.getVin())).thenReturn(deleteAuto);

        Assertions.assertTrue(autosService.deleteAuto(deleteAuto.getVin()));
        verify(autosRepository).delete(any(Auto.class));
    }

    @Test
    void deleteInvalidVinAuto() {
        String deleteVin = mockAutos.get(0).getVin();

        when(autosRepository.findFirstByVin(deleteVin)).thenReturn(null);


        Assertions.assertFalse(autosService.deleteAuto(deleteVin));
    }

}